import http from 'http';
import https from 'https';
import { oAuthResponse, FullCarDetails } from './interfaces/index';

export interface TeslaTrackerProps {
  vehicleId: number;
  username: string;
  password: string;
}

enum apiUrls {
  TESLA_API_V1 = '/api/1',
  TESLA_OAUTH = '/oauth/token',
  TESLA_VEHICLES = '/vehicles/',
  TESLA_DATA = '/data/',
}

class TeslaTracker {
  vehicleId: number;
  username: string;
  password: string;

  accessToken: string;

  baseUrl = 'owner-api.teslamotors.com';


  constructor({ vehicleId, username, password }: TeslaTrackerProps) {
    this.vehicleId = vehicleId;
    this.username = username;
    this.password = password;
  }

  async getTeslaBasic() {

  }

  async getTeslaData() {

  }

  async getBattery() {
    const oAuthData = await this.getAuthToken() as oAuthResponse;
    if (oAuthData.access_token) {
      this.accessToken = oAuthData.access_token;
    }
    
    const fullCarData = await this.getFullCarData() as FullCarDetails;
    if (fullCarData.response.charge_state.battery_level) {
      const chargeState = fullCarData.response.charge_state;
      return {
        battery_level: chargeState.battery_level,
        battery_range: chargeState.battery_range,
        charging_state: chargeState.charging_state,
        time_to_full_charge: chargeState.time_to_full_charge,
        minutes_to_full_charge: chargeState.minutes_to_full_charge,
        charge_rate: chargeState.charge_rate,
      }
    }
  }

  async getFullCarData() {
    return new Promise((resolve, reject) => {
      const options: http.RequestOptions = {
        host: this.baseUrl,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this.accessToken,
        },
        path: `${apiUrls.TESLA_API_V1}${apiUrls.TESLA_VEHICLES}${this.vehicleId}${apiUrls.TESLA_DATA}`,
      }

      const req = https.request(options, res => {
        let responseBody: string = '';
        res.on('data', chunk => responseBody += chunk)

        res.on('end', () => {
          resolve(JSON.parse(responseBody));
        })
      })
      req.on('error', err => reject(err))
      req.end();
    });
  }

  async getAuthToken() {
    return new Promise((resolve, reject) => {
      const data = JSON.stringify(
        {
          password: this.password,
          email: this.username,
          client_secret: 'c7257eb71a564034f9419ee651c7d0e5f7aa6bfbd18bafb5c5c033b093bb2fa3',
          client_id: '81527cff06843c8634fdc09e8ac0abefb46ac849f38fe1e431c2ef2106796384',
          grant_type: 'password'
        }
      );

      const options: http.RequestOptions = {
        host: this.baseUrl,
        port: 443,
        headers: {
          'Content-Type': 'application/json',
        },
        path:  apiUrls.TESLA_OAUTH,
        method: 'POST'
      }

      const req = https.request(options, res => {
        let responseBody: string = '';
        res.on('data', chunk => responseBody += chunk)

        res.on('end', () => {
          resolve(JSON.parse(responseBody));
        })
      })
      req.on('error', err => reject(err))
      req.write(data);
      req.end();
    });
  }
}

export default TeslaTracker;