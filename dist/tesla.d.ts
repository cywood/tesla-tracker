export interface TeslaTrackerProps {
    vehicleId: number;
    username: string;
    password: string;
}
declare class TeslaTracker {
    vehicleId: number;
    username: string;
    password: string;
    accessToken: string;
    baseUrl: string;
    constructor({ vehicleId, username, password }: TeslaTrackerProps);
    getTeslaBasic(): Promise<void>;
    getTeslaData(): Promise<void>;
    getBattery(): Promise<{
        battery_level: number;
        battery_range: number;
        charging_state: string;
        time_to_full_charge: number;
        minutes_to_full_charge: number;
        charge_rate: number;
    }>;
    getFullCarData(): Promise<unknown>;
    getAuthToken(): Promise<unknown>;
}
export default TeslaTracker;
