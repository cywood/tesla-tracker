"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const https_1 = __importDefault(require("https"));
var apiUrls;
(function (apiUrls) {
    apiUrls["TESLA_API_V1"] = "/api/1";
    apiUrls["TESLA_OAUTH"] = "/oauth/token";
    apiUrls["TESLA_VEHICLES"] = "/vehicles/";
    apiUrls["TESLA_DATA"] = "/data/";
})(apiUrls || (apiUrls = {}));
class TeslaTracker {
    constructor({ vehicleId, username, password }) {
        this.baseUrl = 'owner-api.teslamotors.com';
        this.vehicleId = vehicleId;
        this.username = username;
        this.password = password;
    }
    getTeslaBasic() {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    getTeslaData() {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    getBattery() {
        return __awaiter(this, void 0, void 0, function* () {
            const oAuthData = yield this.getAuthToken();
            if (oAuthData.access_token) {
                this.accessToken = oAuthData.access_token;
            }
            const fullCarData = yield this.getFullCarData();
            if (fullCarData.response.charge_state.battery_level) {
                const chargeState = fullCarData.response.charge_state;
                return {
                    battery_level: chargeState.battery_level,
                    battery_range: chargeState.battery_range,
                    charging_state: chargeState.charging_state,
                    time_to_full_charge: chargeState.time_to_full_charge,
                    minutes_to_full_charge: chargeState.minutes_to_full_charge,
                    charge_rate: chargeState.charge_rate,
                };
            }
        });
    }
    getFullCarData() {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                const options = {
                    host: this.baseUrl,
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this.accessToken,
                    },
                    path: `${apiUrls.TESLA_API_V1}${apiUrls.TESLA_VEHICLES}${this.vehicleId}${apiUrls.TESLA_DATA}`,
                };
                const req = https_1.default.request(options, res => {
                    let responseBody = '';
                    res.on('data', chunk => responseBody += chunk);
                    res.on('end', () => {
                        resolve(JSON.parse(responseBody));
                    });
                });
                req.on('error', err => reject(err));
                req.end();
            });
        });
    }
    getAuthToken() {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                const data = JSON.stringify({
                    password: this.password,
                    email: this.username,
                    client_secret: 'c7257eb71a564034f9419ee651c7d0e5f7aa6bfbd18bafb5c5c033b093bb2fa3',
                    client_id: '81527cff06843c8634fdc09e8ac0abefb46ac849f38fe1e431c2ef2106796384',
                    grant_type: 'password'
                });
                const options = {
                    host: this.baseUrl,
                    port: 443,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    path: apiUrls.TESLA_OAUTH,
                    method: 'POST'
                };
                const req = https_1.default.request(options, res => {
                    let responseBody = '';
                    res.on('data', chunk => responseBody += chunk);
                    res.on('end', () => {
                        resolve(JSON.parse(responseBody));
                    });
                });
                req.on('error', err => reject(err));
                req.write(data);
                req.end();
            });
        });
    }
}
exports.default = TeslaTracker;
